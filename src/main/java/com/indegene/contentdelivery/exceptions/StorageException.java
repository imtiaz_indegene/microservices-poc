package com.indegene.contentdelivery.exceptions;

/**
 * Created by imirza on 12/19/2016.
 */
public class StorageException extends RuntimeException {

    public StorageException(String message) {
        super(message);
    }

    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }
}