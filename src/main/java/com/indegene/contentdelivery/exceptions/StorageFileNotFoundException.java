package com.indegene.contentdelivery.exceptions;

/**
 * Created by imirza on 12/19/2016.
 */


public class StorageFileNotFoundException extends StorageException {

    public StorageFileNotFoundException(String message) {
        super(message);
    }

    public StorageFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}