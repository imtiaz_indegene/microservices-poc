package com.indegene.contentdelivery.controllers;


import com.indegene.contentdelivery.exceptions.StorageFileNotFoundException;
import com.indegene.contentdelivery.services.FileService;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

@RestController
public class FileController {

    private final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    FileService hdfsFileAccess;

    private String defaultFile = "index.html";


    @RequestMapping("/api/v1/files/{fileuuid:.+}")
    @ResponseBody
    public void getFileContents(HttpServletResponse response,
                                @PathVariable("fileuuid") String fileNameWithUUID) throws Exception {
        response.addHeader("Content-disposition", "inline;filename="+fileNameWithUUID);
        response.setContentType("application/octet-stream");

        logger.info("filePath = " + fileNameWithUUID);
        IOUtils.copy(hdfsFileAccess.downStreamV1( fileNameWithUUID, 0L), response.getOutputStream());

        response.flushBuffer();
    }


    @RequestMapping(value = "/api/v1/files/upload", method = RequestMethod.POST,
            produces = { "application/json" } )
    @ResponseBody
    public HashMap<String, String> handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException {
    HashMap<String, String> response = new HashMap<>();
         response.put( "fileUploadMessage",
                "You successfully uploaded " + file.getOriginalFilename() + "!");

        String uuidFileName =  UUID.randomUUID().toString()+"_"+file.getOriginalFilename();

        hdfsFileAccess.upstream( file.getInputStream() , uuidFileName);

        response.put("message", "File Uploaded Successfully");
        response.put("generatedFileName", uuidFileName );

        return response;
    }

    @RequestMapping(path = "/webcontent/v1", method = RequestMethod.GET)
    @ResponseBody
    public void getDefault(HttpServletResponse response) throws Exception {
        response.setContentType(ContentType.TEXT_HTML.getMimeType());

        IOUtils.copy(hdfsFileAccess.downStreamV1(defaultFile, 0L), response.getOutputStream());

        response.flushBuffer();
    }



    @RequestMapping(path = "/webcontent/v1/**", method = RequestMethod.GET)
    @ResponseBody
    public void getFile(HttpServletResponse response, HttpServletRequest request) throws Exception {
        String filePath = request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE)
                .toString().replaceFirst("/webcontent/v1", "");
        String range = request.getHeader("Range");
        hdfsFileAccess.getWebContent(response, filePath, range);
    }


    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }


    @RequestMapping("/hello")
    public String index() {
        return "Greetings from Spring Boot!";
    }


}
