package com.indegene.contentdelivery;


import com.indegene.contentdelivery.configurations.HDFSConfig;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.io.IOException;
import java.security.PrivilegedExceptionAction;

@SpringBootApplication
@ComponentScan

public class Application {

    private final Logger logger = LoggerFactory.getLogger(Application.class);

    @Autowired
    private HDFSConfig configBean;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    FileSystem createFileSystem() throws IOException, InterruptedException {
        System.setProperty("hadoop.home.dir", configBean.getHadoopHomeDir());
        UserGroupInformation ug = UserGroupInformation.createRemoteUser("hduser");

        return ug.doAs(new PrivilegedExceptionAction<FileSystem>() {
            public FileSystem run() throws Exception {
                Configuration hadoopConfig = new Configuration();
                hadoopConfig.set("fs.defaultFS", configBean.getFsDefaultFS());
                hadoopConfig.set("hadoop.home.dir", configBean.getHadoopHomeDir());
                hadoopConfig.set("mapred.child.java.opts", configBean.getMapredChildJavaOpts());
                hadoopConfig.set("mapred.compress.map.output", configBean.getMapredCompressMapOutput());
                hadoopConfig.set("mapred.map.output.compression.codec",
                        configBean.getMapredMapOutputCompressionCodec());
                hadoopConfig.set("io.sort.mb", configBean.getIoSortMb());
                hadoopConfig.set("mapred.job.reuse.jvm.num.tasks",
                        configBean.getMapredJobReuseJvmNumTasks());
                hadoopConfig.set("mapred.max.split.size", configBean.getMapredMaxSplitSize());
                hadoopConfig.set("mapred.min.split.size", configBean.getMapredMinSplitSize());

                hadoopConfig.set("fs.hdfs.impl",
                        DistributedFileSystem.class.getName()
                );
                hadoopConfig.set("fs.file.impl",
                        LocalFileSystem.class.getName()
                );

                logger.info("################### HDFS config loaded for filesystem ");

                return FileSystem.get(hadoopConfig);
            }
        });
    }
}
