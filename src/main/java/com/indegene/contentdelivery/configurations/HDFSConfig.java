package com.indegene.contentdelivery.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by imirza on 12/16/2016.
 */
@Component
@PropertySource(value = "hdfs.conf")
public class HDFSConfig {

    @Value("${fs.defaultFS}")
    private String fsDefaultFS;

    @Value("${hadoop.user}")
    private String hadoopUser;

    @Value("${hadoop.home.dir}")
    private String hadoopHomeDir;

     @Value("${mapred.child.java.opts}")
    private String  mapredChildJavaOpts;

    @Value("${mapred.compress.map.output}")
    private String mapredCompressMapOutput;

    @Value("${mapred.map.output.compression.codec}")
    private String  mapredMapOutputCompressionCodec;

    @Value("${io.sort.mb}")
    private String ioSortMb;

    @Value("${mapred.job.reuse.jvm.num.tasks}")
    private String mapredJobReuseJvmNumTasks;

    @Value("${mapred.min.split.size}")
    private String mapredMinSplitSize;

    @Value("${mapred.max.split.size")
    private String mapredMaxSplitSize;

    public String getFsDefaultFS() {
        return fsDefaultFS;
    }

    public String getHadoopUser() {
        return hadoopUser;
    }

    public String getHadoopHomeDir() {
        return hadoopHomeDir;
    }

    public String getMapredChildJavaOpts() {
        return mapredChildJavaOpts;
    }

    public String getMapredCompressMapOutput() {
        return mapredCompressMapOutput;
    }

    public String getMapredMapOutputCompressionCodec() {
        return mapredMapOutputCompressionCodec;
    }

    public String getIoSortMb() {
        return ioSortMb;
    }

    public String getMapredJobReuseJvmNumTasks() {
        return mapredJobReuseJvmNumTasks;
    }

    public String getMapredMinSplitSize() {
        return mapredMinSplitSize;
    }

    public String getMapredMaxSplitSize() {
        return mapredMaxSplitSize;
    }
}
