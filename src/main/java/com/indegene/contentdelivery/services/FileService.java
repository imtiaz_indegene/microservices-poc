package com.indegene.contentdelivery.services;


import com.indegene.contentdelivery.configurations.HDFSConfig;
import com.indegene.contentdelivery.exceptions.StorageFileNotFoundException;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.hadoop.fs.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class FileService {


    @Autowired
    private HDFSConfig configBean;

    private String directory = "/";

    @Autowired
    FileSystem fileSystem;

    public void deleteHDFSFile(String xpath){

        Path path = new Path(Paths.get(directory, xpath).toString());
        try {
            if (fileSystem.exists(path)) {
                fileSystem.delete(path, true);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Async
    public void upstream(InputStream is, String filePath) throws IOException {


        try {
            FSDataOutputStream fsout = fileSystem.create(
                    new Path(Paths.get("", filePath).toString()), true);

            while(is.available() > 0) {
                byte[] bytes = org.apache.commons.io.IOUtils.toByteArray(is);
                fsout.write(bytes);
            }
            fsout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Resource loadAsResource(String filename) {
        Path file = new Path(Paths.get("/my_storage", filename).toString());
        Resource resource = null;
        try {
            resource = new UrlResource(file.toUri());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if(resource.exists() || resource.isReadable()) {
            return resource;
        }
        else {
            throw new StorageFileNotFoundException("Could not read file: " + filename);

        }
    }

    @Async
    public void getWebContent(final HttpServletResponse response, final String filePath, final String range) throws Exception {
        if(range != null && !range.isEmpty()) {
            String offsetValue = range.substring(range.indexOf('=') + 1, range.indexOf('-'));
            String lengthValue = range.substring(range.indexOf('-') + 1);

            Long offset = 0L;
            Long length = null;

            if (offsetValue != null && !offsetValue.isEmpty()) {
                offset = java.lang.Long.parseLong(offsetValue);
            }

            if (lengthValue != null && !lengthValue.isEmpty()) {
                length = java.lang.Long.parseLong(lengthValue);
            }

            Long contentLength = checkFileV1(new Path(Paths.get(configBean.getHadoopHomeDir(), filePath).toString()), offset, length);

            if (contentLength != -1L) {
                InputStream is = downStreamV1(Paths.get(configBean.getHadoopHomeDir(), filePath).toString(), offset);

                response.addHeader("Accept-Ranges", "bytes");
                response.addHeader("Content-Range",
                        "bytes " + offset + "-" + (contentLength - 1) + "/"
                                + contentLength);

                response.setContentType(URLConnection.guessContentTypeFromStream(is));
                response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);

                IOUtils.copy(is, response.getOutputStream());

                response.flushBuffer();
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        } else {
            InputStream is = downStreamV1(Paths.get(configBean.getHadoopHomeDir(), filePath).toString(), 0L);
            System.out.println("CONTENT TYPE: " + Files.probeContentType(Paths.get(filePath).getFileName()));
            response.setContentType(Files.probeContentType(Paths.get(filePath).getFileName()));

            IOUtils.copy(is, response.getOutputStream());

            response.flushBuffer();
        }
    }

    public Long checkFileV1(final Path filePath, final Long offset, final Long length) throws Exception {
        Long contentLength = 0L;

        if (fileSystem.exists(filePath) && !fileSystem.isDirectory(filePath)) {
            FileStatus status = fileSystem.getFileStatus(filePath);

            contentLength = status.getLen();

            if (offset >= contentLength || (length != null && length < offset)) {
                contentLength = -1L;
            } else {
                if (length != null && length > 0) {
                    contentLength = length;
                }
            }
        }

        return contentLength;
    }

    @Async
    public InputStream downStreamV1(final String filePath, final Long offset) throws Exception {
        FSDataInputStream fsin = fileSystem.open(
                new Path(filePath));
        fsin.seek(offset);

        return fsin;
    }

}
